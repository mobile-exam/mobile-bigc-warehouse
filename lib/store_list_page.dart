import 'package:flutter/material.dart';
import 'package:mobile_exam/model/store_viewmodel.dart';
import 'package:provider/provider.dart';
import 'package:mobile_exam/model/store.dart';

class StoreListPage extends StatefulWidget {
  final double latitude;
  final double longitude;

  const StoreListPage({
    Key? key,
    required this.latitude,
    required this.longitude,
  }) : super(key: key);

  @override
  State<StoreListPage> createState() => _StoreListPageState();
}

class _StoreListPageState extends State<StoreListPage> {
  @override
  Widget build(BuildContext context) {
    final vm = context.read<StoreViewModel>();

    return Scaffold(
      appBar: AppBar(
        title: const Text("Warehouses or Something"),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: FutureBuilder<List<WarehouseStore>?>(
        future: context.read<StoreViewModel>().getStores(
          widget.latitude,
          widget.longitude,
        ),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
            return const Center(child: Text('No stores available.'));
          } else {
            final stores = snapshot.data!;

            return ListView.builder(
              itemCount: stores.length,
              itemBuilder: (context, index) {
                final store = stores[index];
                return Card(
                  margin: const EdgeInsets.all(10.0),
                  elevation: 2.0,
                  child: ListTile(
                    contentPadding: const EdgeInsets.all(20.0),
                    title: Text(
                      '  ${store.warehouseNameTh}',
                      style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    subtitle: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Text(
                            '${((store.distance ?? 0) / 1000).toStringAsFixed(2)} กม.  |  ',
                            style: const TextStyle(color: Colors.lightGreen),
                          ),
                          const Icon(Icons.schedule, size: 16.0),
                          Text(
                            ' เปิดให้บริการ ${store.openTime != null ? vm.getTime(store.openTime!) : 'Unknown'} - '
                                '${store.closeTime != null ? vm.getTime(store.closeTime!) : 'Unknown'} น.',
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            );
          }
        },
      ),
    );
  }
}
