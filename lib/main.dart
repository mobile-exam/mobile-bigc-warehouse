import 'package:flutter/material.dart';
import 'package:mobile_exam/home.dart';

import 'package:provider/provider.dart';

import 'model/store_viewmodel.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [ChangeNotifierProvider(create: (_) => StoreViewModel())],
    child: MaterialApp(
      title: "titleeeeeeee",
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
      ),
      home: HomeWidget(),
    )
    );
  }
}
