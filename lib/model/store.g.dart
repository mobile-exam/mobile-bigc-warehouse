// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'store.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Stores _$StoresFromJson(Map<String, dynamic> json) => Stores(
      warehouseStores: (json['warehouse_stores'] as List<dynamic>?)
          ?.map((e) => WarehouseStore.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$StoresToJson(Stores instance) => <String, dynamic>{
      'warehouse_stores':
          instance.warehouseStores?.map((e) => e.toJson()).toList(),
    };

WarehouseStore _$WarehouseStoreFromJson(Map<String, dynamic> json) =>
    WarehouseStore(
      warehouseNameTh: json['warehouse_name_th'] as String?,
      distance: (json['distance'] as num?)?.toDouble(),
      openTime: json['open_time'] as String?,
      closeTime: json['close_time'] as String?,
    );

Map<String, dynamic> _$WarehouseStoreToJson(WarehouseStore instance) =>
    <String, dynamic>{
      'warehouse_name_th': instance.warehouseNameTh,
      'distance': instance.distance,
      'open_time': instance.openTime,
      'close_time': instance.closeTime,
    };
