import 'package:json_annotation/json_annotation.dart';

part 'store.g.dart';

@JsonSerializable(explicitToJson: true, createToJson: true, fieldRename: FieldRename.snake)
class Stores {
  List<WarehouseStore>? warehouseStores;
  Stores({this.warehouseStores});
  factory Stores.fromJson(Map<String, dynamic> json) => _$StoresFromJson(json);
  Map<String, dynamic> toJson() => _$StoresToJson(this);
}

@JsonSerializable(explicitToJson: true, createToJson: true, fieldRename: FieldRename.snake)
class WarehouseStore {
  String? warehouseNameTh;
  double? distance;
  String? openTime;
  String? closeTime;

  WarehouseStore({this.warehouseNameTh, this.distance, this.openTime, this.closeTime});
  factory WarehouseStore.fromJson(Map<String, dynamic> json) => _$WarehouseStoreFromJson(json);
  Map<String, dynamic> toJson() => _$WarehouseStoreToJson(this);
}
