

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:mobile_exam/model/store.dart';
import 'package:mobile_exam/service/api_manager.dart';
import 'package:mobile_exam/service/store_service.dart';


class StoreViewModel extends ChangeNotifier {
  WarehouseStore? warehouseStore;
  StoreService? storeService;
  ApiManager? apiManager;



  String getTime(String dateTimeVa) {
    List<String> parts = dateTimeVa.split(' ');
    if (parts.length >= 2) {
      String timePart = parts[1];
      List<String> timeComponents = timePart.split(':');
      if (timeComponents.length >= 3) {
        int hours = int.tryParse(timeComponents[0]) ?? 0;
        int minutes = int.tryParse(timeComponents[1]) ?? 0;
        String formattedHours = hours.toString().padLeft(2, '0');
        String formattedMinutes = minutes.toString().padLeft(2, '0');

        return '$formattedHours:$formattedMinutes';
      }
    }
    return '';
  }

  Future<List<WarehouseStore>?> getStores(double latitude, double longitude) async {
    var res = await (storeService ?? StoreService()).getStores(latitude, longitude);
    if (res.data != null) {
      return res.data; // Return a list with a single store
    }
    return [];
  }





}


