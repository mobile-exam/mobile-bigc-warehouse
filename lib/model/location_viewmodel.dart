import 'package:flutter/material.dart';
import 'package:mobile_exam/model/location.dart';

class LocationViewModel extends ChangeNotifier {
  final Location _location = Location();
  final RegExp _numericRegExp = RegExp(r'^-?\d*\.?\d+$');

  bool isInputValid(String input) {
    return _numericRegExp.hasMatch(input);
  }

  void setLatitude(String value) {
    if (isInputValid(value)) {
      _location.latitude = double.tryParse(value);
    }
  }

  void setLongitude(String value) {
    if (isInputValid(value)) {
      _location.longitude = double.tryParse(value);
    }
  }
  Location get location => _location;
}
