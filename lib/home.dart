import 'package:flutter/material.dart';
import 'package:mobile_exam/store_list_page.dart';
import 'package:provider/provider.dart';
import 'model/location_viewmodel.dart';

class HomeWidget extends StatelessWidget {
  const HomeWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<LocationViewModel>(
          create: (_) => LocationViewModel(),
        ),
      ],
      child: _HomeWidgetContent(),
    );
  }
}

class _HomeWidgetContent extends StatefulWidget {
  @override
  State<_HomeWidgetContent> createState() => _HomeWidgetContentState();
}

class _HomeWidgetContentState extends State<_HomeWidgetContent> {
  TextEditingController latitudeController = TextEditingController();
  TextEditingController longitudeController = TextEditingController();
  FocusNode fc1 = FocusNode();
  FocusNode fc2 = FocusNode();

  bool isValidLatitude = true;
  bool isValidLongitude = true;

  @override
  Widget build(BuildContext context) {
    final vm = context.read<LocationViewModel>();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Location'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            TextField(
              focusNode: fc1,
              controller: latitudeController,
              onChanged: (value) {
                setState(() {
                  isValidLatitude = vm.isInputValid(value);
                });
                vm.setLatitude(value);
              },
              keyboardType: const TextInputType.numberWithOptions(decimal: true),
              decoration: InputDecoration(
                labelText: 'Enter latitude',
                errorText: isValidLatitude || latitudeController.text.isEmpty
                    ? null : 'Invalid input',
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: isValidLatitude || latitudeController.text.isEmpty ? Colors.black : Colors.red,
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20.0),
            TextField(
              focusNode: fc2,
              controller: longitudeController,
              onChanged: (value) {
                setState(() {
                  isValidLongitude = vm.isInputValid(value);
                });
                vm.setLongitude(value);
              },
              keyboardType: const TextInputType.numberWithOptions(decimal: true),
              decoration: InputDecoration(
                labelText: 'Enter longitude',
                errorText: isValidLongitude || longitudeController.text.isEmpty
                    ? null
                    : 'Invalid input',
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: isValidLongitude || longitudeController.text.isEmpty ? Colors.black : Colors.red,
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20.0),
            SizedBox(
              width: double.infinity,
              height: 50.0,
              child: ElevatedButton(
                onPressed: (isValidLatitude && isValidLongitude &&
                    latitudeController.text.isNotEmpty &&
                    longitudeController.text.isNotEmpty)
                    ? () {
                  fc1.unfocus();
                  fc2.unfocus();
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => StoreListPage(
                        latitude: vm.location.latitude ?? 0.0,
                        longitude: vm.location.longitude ?? 0.0,
                      ),
                    ),
                  ).then((_) {
                    latitudeController.clear();
                    longitudeController.clear();
                    setState(() {
                      isValidLatitude = true;
                      isValidLongitude = true;
                    });
                  });
                }
                    : null,
                child: const Text("SUBMIT"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

