import 'package:dio/dio.dart';
import 'package:mobile_exam/service/resourse.dart';
import '../model/store.dart';
import 'api_manager.dart';

abstract class StoreApi {
  Future<Resource<List<WarehouseStore>>> getStores(double latitude, double longitude);
}

class StoreService implements StoreApi {
  late Dio dio;
  late ApiManager apiManager;

  StoreService() {
    dio = Dio();
    apiManager = ApiManager(dio);
  }

  @override
  Future<Resource<List<WarehouseStore>>> getStores(double latitude, double longitude) async {
    var body = {'latitude': latitude, 'longitude': longitude, 'count': 300};
    try {
      final res = await apiManager.getStores(body);
      final List<dynamic>? warehouseStoresJson = res.data?['warehouses']['warehouse_stores'];
      if (warehouseStoresJson == null) {
        return Resource.error(error: 'No warehouse stores available.', code: 'no_data');
      }
      final List<WarehouseStore> warehouseStores = warehouseStoresJson.map((e) => WarehouseStore.fromJson(e)).toList();
      return Resource.success(data: warehouseStores, code: 'success');
    } catch (e) {
      return Resource.error(error: e.toString(), code: 'ain\'t 200');
    }
  }
}
