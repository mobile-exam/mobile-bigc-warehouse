class Resource<T> {
  final String? code;
  final T? data;
  Resource({this.code, this.data});
  static Resource<T> success<T>({required T data, required String code}) {
    return Resource<T>(code: code, data: data);
  }
  static Resource<T> error<T>({ required String code, required String error, T? data}) {
    return Resource<T>(code: code, data: data);
  }
}