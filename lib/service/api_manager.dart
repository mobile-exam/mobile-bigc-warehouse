import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';

part 'api_manager.g.dart';

@RestApi(baseUrl: 'https://openapi.bigc-cs.com')
abstract class ApiManager {
  factory ApiManager(Dio dio, {String baseUrl}) = _ApiManager;

  @POST('/composite/v2/warehouses')
  Future<ResponseGenericData> getStores(@Body() Map<String, dynamic> body);
}

@JsonSerializable(createToJson: false)
class ResponseGenericData {
  String? code;
  dynamic data;
  ResponseGenericData({required this.code, required this.data});
  factory ResponseGenericData.fromJson(Map<String, dynamic> json) => _$ResponseGenericDataFromJson(json);
}
