// Mocks generated by Mockito 5.4.0 from annotations
// in mobile_exam/test/store_service_test.dart.
// Do not manually edit this file.

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'dart:async' as _i6;

import 'package:dio/dio.dart' as _i2;
import 'package:mobile_exam/model/store.dart' as _i7;
import 'package:mobile_exam/service/api_manager.dart' as _i3;
import 'package:mobile_exam/service/resourse.dart' as _i4;
import 'package:mobile_exam/service/store_service.dart' as _i5;
import 'package:mockito/mockito.dart' as _i1;

// ignore_for_file: type=lint
// ignore_for_file: avoid_redundant_argument_values
// ignore_for_file: avoid_setters_without_getters
// ignore_for_file: comment_references
// ignore_for_file: implementation_imports
// ignore_for_file: invalid_use_of_visible_for_testing_member
// ignore_for_file: prefer_const_constructors
// ignore_for_file: unnecessary_parenthesis
// ignore_for_file: camel_case_types
// ignore_for_file: subtype_of_sealed_class

class _FakeDio_0 extends _i1.SmartFake implements _i2.Dio {
  _FakeDio_0(
    Object parent,
    Invocation parentInvocation,
  ) : super(
          parent,
          parentInvocation,
        );
}

class _FakeApiManager_1 extends _i1.SmartFake implements _i3.ApiManager {
  _FakeApiManager_1(
    Object parent,
    Invocation parentInvocation,
  ) : super(
          parent,
          parentInvocation,
        );
}

class _FakeResource_2<T> extends _i1.SmartFake implements _i4.Resource<T> {
  _FakeResource_2(
    Object parent,
    Invocation parentInvocation,
  ) : super(
          parent,
          parentInvocation,
        );
}

class _FakeResponseGenericData_3 extends _i1.SmartFake
    implements _i3.ResponseGenericData {
  _FakeResponseGenericData_3(
    Object parent,
    Invocation parentInvocation,
  ) : super(
          parent,
          parentInvocation,
        );
}

/// A class which mocks [StoreService].
///
/// See the documentation for Mockito's code generation for more information.
class MockStoreService extends _i1.Mock implements _i5.StoreService {
  MockStoreService() {
    _i1.throwOnMissingStub(this);
  }

  @override
  _i2.Dio get dio => (super.noSuchMethod(
        Invocation.getter(#dio),
        returnValue: _FakeDio_0(
          this,
          Invocation.getter(#dio),
        ),
      ) as _i2.Dio);
  @override
  set dio(_i2.Dio? _dio) => super.noSuchMethod(
        Invocation.setter(
          #dio,
          _dio,
        ),
        returnValueForMissingStub: null,
      );
  @override
  _i3.ApiManager get apiManager => (super.noSuchMethod(
        Invocation.getter(#apiManager),
        returnValue: _FakeApiManager_1(
          this,
          Invocation.getter(#apiManager),
        ),
      ) as _i3.ApiManager);
  @override
  set apiManager(_i3.ApiManager? _apiManager) => super.noSuchMethod(
        Invocation.setter(
          #apiManager,
          _apiManager,
        ),
        returnValueForMissingStub: null,
      );
  @override
  _i6.Future<_i4.Resource<List<_i7.WarehouseStore>>> getStores(
    double? latitude,
    double? longitude,
  ) =>
      (super.noSuchMethod(
        Invocation.method(
          #getStores,
          [
            latitude,
            longitude,
          ],
        ),
        returnValue: _i6.Future<_i4.Resource<List<_i7.WarehouseStore>>>.value(
            _FakeResource_2<List<_i7.WarehouseStore>>(
          this,
          Invocation.method(
            #getStores,
            [
              latitude,
              longitude,
            ],
          ),
        )),
      ) as _i6.Future<_i4.Resource<List<_i7.WarehouseStore>>>);
}

/// A class which mocks [ApiManager].
///
/// See the documentation for Mockito's code generation for more information.
class MockApiManager extends _i1.Mock implements _i3.ApiManager {
  MockApiManager() {
    _i1.throwOnMissingStub(this);
  }

  @override
  _i6.Future<_i3.ResponseGenericData> getStores(Map<String, dynamic>? body) =>
      (super.noSuchMethod(
        Invocation.method(
          #getStores,
          [body],
        ),
        returnValue: _i6.Future<_i3.ResponseGenericData>.value(
            _FakeResponseGenericData_3(
          this,
          Invocation.method(
            #getStores,
            [body],
          ),
        )),
      ) as _i6.Future<_i3.ResponseGenericData>);
}

/// A class which mocks [Resource].
///
/// See the documentation for Mockito's code generation for more information.
class MockResource<T> extends _i1.Mock implements _i4.Resource<T> {
  MockResource() {
    _i1.throwOnMissingStub(this);
  }
}
