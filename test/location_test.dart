
import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_exam/model/location_viewmodel.dart';

void main() {
  group('LocationViewModel Tests', () {
    late LocationViewModel viewModel;

    setUp(() {
      viewModel = LocationViewModel();
    });

    test('isInputValid returns true for valid input', () {
      expect(viewModel.isInputValid('123'), true);
      expect(viewModel.isInputValid('-123.45'), true);
    });

    test('isInputValid returns false for invalid input', () {
      expect(viewModel.isInputValid('abc'), false);
      expect(viewModel.isInputValid('12a'), false);
      expect(viewModel.isInputValid(''), false);
    });

    test('setLatitude sets the latitude correctly', () {
      const value = '12.345';
      viewModel.setLatitude(value);
      expect(viewModel.location.latitude, double.tryParse(value));
    });

    test('setLongitude sets the longitude correctly', () {
      const value = '-67.89';
      viewModel.setLongitude(value);
      expect(viewModel.location.longitude, double.tryParse(value));
    });
  });
}
