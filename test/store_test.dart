import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_exam/model/store.dart';
import 'package:mobile_exam/model/store_viewmodel.dart';
import 'package:mobile_exam/service/api_manager.dart';
import 'package:mobile_exam/service/resourse.dart';
import 'package:mobile_exam/service/store_service.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'store_test.mocks.dart';

@GenerateMocks([ApiManager, StoreService])
void main() {
  group('StoreViewModel Tests', () {
    late StoreViewModel storeViewModel;
    late MockStoreService mockStoreService;
    late MockApiManager mockApiManager;
    const double latitude = 13.5;
    const double longitude = 100.5;

    setUp(() {
      mockStoreService = MockStoreService();
      mockApiManager = MockApiManager();

      storeViewModel = StoreViewModel();
      storeViewModel.storeService = mockStoreService;
      storeViewModel.apiManager = mockApiManager;
    });

    test('getStores returns warehouse stores', () async {
      when(mockStoreService.getStores(any, any)).thenAnswer((_) async {
        return Resource(
            data: [WarehouseStore(warehouseNameTh: 'Store1'), WarehouseStore(warehouseNameTh: 'Store2')],
            code: 'success');
      });
      storeViewModel.storeService = mockStoreService;
      final stores = await storeViewModel.getStores(latitude, longitude);
      expect(stores, isA<List<WarehouseStore>>());
      expect(stores?.length, 2);

      // Verify that getStores was called on the mock service
      verify(mockStoreService.getStores(latitude, longitude)).called(1);
    });
    test('getStores returns empty list when data is null', () async {
      when(mockStoreService.getStores(any, any)).thenAnswer((_) async =>
          Resource(
          data: [],
          code: 'code'));
      final stores = await storeViewModel.getStores(latitude, longitude);
      expect(stores, []);
      verify(mockStoreService.getStores(latitude, longitude)).called(1);
    });
    test('getTime returns formatted time', () {
      final dateTime = '2023-09-22 14:30:00';
      final formattedTime = storeViewModel.getTime(dateTime);
      expect(formattedTime, '14:30');
    });

    test('getTime returns empty string for invalid input', () {
      final invalidDateTime = 'Invalid DateTime';
      final formattedTime = storeViewModel.getTime(invalidDateTime);
      expect(formattedTime, '');
    });
  });
}
