import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_exam/model/store.dart';
import 'package:mobile_exam/service/api_manager.dart';
import 'package:mobile_exam/service/resourse.dart';
import 'package:mobile_exam/service/store_service.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'store_service_test.mocks.dart';

@GenerateMocks([StoreService, ApiManager, Resource])
void main() {
    late StoreService storeService;
    late MockApiManager mockApiManager;

    const double latitude = 13.5;
    const double longitude = 100.5;

    setUp(() {
        mockApiManager = MockApiManager();
        storeService = StoreService();
        storeService.apiManager = mockApiManager;
    });

    test('getStores returns warehouse stores', () async {
        final responseData = {
            'warehouses': {
                'warehouse_stores': [
                    {'warehouse_name_th': 'บิ๊กซี เพชรบุรี'},
                    {'warehouse_name_th': 'บิ๊กซี เอกมัย'},
                ]
            }
        };

        when(mockApiManager.getStores(any)).thenAnswer((_) async {
            return ResponseGenericData(data: responseData, code: 'success');
        });
        final resource = await storeService.getStores(13.5, 100.5);
        expect(resource.data, isA<List<WarehouseStore>>());
        expect(resource.data?.length, 2);
        expect(resource.code, 'success');
    });

    test('getStores returns error on DioError', () async {
        when(mockApiManager.getStores(any)).thenThrow(DioError(
            requestOptions: RequestOptions(path: ''),
            response: Response(requestOptions: RequestOptions(path: '')),
        ));

        final resource = await storeService.getStores(13.5, 100.5);

        expect(resource, isA<Resource<List<WarehouseStore>>>());
        expect(resource.data, null);
        expect(resource.code, 'ain\'t 200');
    });


    test('getStores returns an empty list when warehouse stores are empty', () async {
        final responseData = {'warehouses': {'warehouse_stores': []}};
        when(mockApiManager.getStores(any)).thenAnswer(
                (_) async => ResponseGenericData(data: responseData, code: 'success'),
        );
        final resource = await storeService.getStores(latitude, longitude);
        expect(resource, isA<Resource<List<WarehouseStore>>>());
        expect(resource.data, []);
        expect(resource.code, 'success');
    });
}
